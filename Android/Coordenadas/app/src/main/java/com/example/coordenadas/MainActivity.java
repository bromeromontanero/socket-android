package com.example.coordenadas;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.StatFs;
import android.os.StrictMode;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    TextView ubicacion;
    TextView rendimiento;
    RetrofitClient NextClient;
    ApiService NextService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        //Se inicializan los controles
        ubicacion =(TextView) findViewById(R.id.ubicacion_id);
        rendimiento=(TextView) findViewById(R.id.estatus_id);

        //Se validan los permisos de uso de ubicación
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
        } else {
            locationStart();
        }
        RetrofitInit();

        //Se valida la bateria en uso
        IntentFilter iFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = registerReceiver(null, iFilter);

        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        float bateria = (level / (float)scale)*100;

        //Validación de espacio de dispositivo
        long espacioTotal,espacioOcupado,espacioLibre;
        espacioTotal=totalSpace();
        espacioOcupado=busySpace();
        espacioLibre=freeSpace();

        //Validación de memoria ram
        long memoriaTotal, memoriaOcupada, memoriaDisponible;
        memoriaTotal=totalMemory();
        memoriaOcupada=busyMemory();
        memoriaDisponible=freeMemory();

        rendimiento.setText(bateria+"%"+','+espacioTotal+" GB,"+espacioOcupado+" GB,"+espacioLibre+" GB,"+memoriaTotal+ " MB," +memoriaOcupada+ " MB," +memoriaDisponible+ " MB");
    }
    //Validación de consumo de WS
    private void consumoWS(){

        Call<ResponseBody> call = NextService.doLogin();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    Toast.makeText(getApplicationContext(),response.body().string().toString(), Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Problemas de conexión. Inténtelo de nuevo"+ t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
    private void RetrofitInit(){
        NextClient = RetrofitClient.getInstance();
        NextService = NextClient.getApiService();
    }

    //Métodos de cálculo de espacio en dispositivo
    public long totalSpace()
    {
        String ruta=getFilesDir().getAbsolutePath();
        StatFs statFs=new StatFs(ruta);
        long total= statFs.getTotalBytes();
        total= (long) (total/(1024.f * 1024.f * 1024.f));
        return total;
    }
    public long freeSpace()
    {
        String ruta=getFilesDir().getAbsolutePath();
        StatFs statFs=new StatFs(ruta);
        long free=statFs.getFreeBytes();
        free= (long) (free/(1024.f * 1024.f * 1024.f));
        return free;
    }
    public long busySpace()
    {
        String ruta=getFilesDir().getAbsolutePath();
        StatFs statFs=new StatFs(ruta);
        long total=statFs.getTotalBytes();
        long free=statFs.getFreeBytes();
        total= (long) (total/(1024.f * 1024.f * 1024.f));
        free= (long) (free/(1024.f * 1024.f * 1024.f));
        long   busy   = total - free;
        return busy;
    }
    //Métodos de rendimiento de memoria RAM
    public long totalMemory()
    {
        ActivityManager activityManager=(ActivityManager)getSystemService(ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo mi=new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(mi);
        long total=mi.totalMem/(1024*1024);
        return total;

    }
    public long busyMemory()
    {
        ActivityManager activityManager=(ActivityManager)getSystemService(ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo mi=new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(mi);
        long busy=(mi.totalMem-mi.availMem)/(1024*1024);
        return busy;
    }
    public long freeMemory()
    {
        ActivityManager activityManager=(ActivityManager)getSystemService(ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo mi=new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(mi);
        long free=mi.availMem/(1024*1024);
        return free;
    }
    //Métodos de los botones de petición
    public void enviaUbicacion(View v){
        /*En este paso se envia la información del TextView que contiene la ubicación y se repite automáticamente cada 30 seg*/
        start();
        consumoWS();
    }
    public void detenerEnvio(View v){
        /*En este paso se detiene el envío automático de coordenadas*/
        stop();
        Toast.makeText(this,"Envío de petición finalizada",Toast.LENGTH_LONG).show();
    }
    //Se establece un hilo de repetición
    public Handler mhandler= new Handler();
    public void start(){
        datos.run();
    }
    public void stop(){
        mhandler.removeCallbacks(datos);
    }

    private Runnable datos= new Runnable() {
        @Override
        public void run() {
         /* Se repite la acción de envío*/
            Enviar en=new Enviar(ubicacion.getText().toString()+','+rendimiento.getText().toString());
            en.execute();
            mhandler.postDelayed(this,30000);
        }
    };
    private void locationStart() {
        LocationManager mlocManager;
        mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Localizacion Local = new Localizacion();
        Local.setMainActivity(this);
        final boolean gpsEnabled = mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!gpsEnabled) {
            Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(settingsIntent);
        }
        if ((ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) && (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
            return;
        }
        mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, (LocationListener) Local);
        mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) Local);
    }
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 1000) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                locationStart();
                return;
            }
        }
    }
    public void setLocation(Location loc) {
        //Validar que existan las coordenadas, la latitud y la longitud
        if (loc.getLatitude() != 0.0 && loc.getLongitude() != 0.0) {
            try {
                //Se asigna las coordenadas, la fecha y la velocidad
                ubicacion.setText(loc.getLatitude()+","+loc.getLongitude()+","+loc.getTime()+","+loc.getSpeed());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

package com.example.coordenadas;

import android.os.AsyncTask;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.net.Socket;

public class Enviar extends AsyncTask<String,Void,Void> {
    Socket s;
    DataOutputStream dt;
    PrintWriter pt;

    String mensaje;

    public Enviar(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    protected Void doInBackground(String... Voids) {
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        try {
            s=new Socket("192.168.0.5",8000);//La IP corresponde al servidor que apunta y el puerto disponible
            pt=new PrintWriter(s.getOutputStream());
            pt.write(mensaje);
            pt.flush();
            pt.close();
            s.close();
        }catch(IOException e) {
            e.printStackTrace();
        }
    }
}
